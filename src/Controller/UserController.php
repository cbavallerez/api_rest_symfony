<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Email;




use App\Entity\User;
use App\Entity\Video;
use App\Services\JwtAuth;

//use Doctrine\Bundle\DoctrineBundle\Registry;
//use 

class UserController extends AbstractController
{


    public function index()
    {

        $user_repo = $this->getDoctrine()->getRepository(User::class);
        $video_repo = $this->getDoctrine()->getRepository(Video::class);


        $users = $user_repo->findAll();
        $videos = $video_repo->findAll();
/*
        foreach ($users as $user) {
           echo "<h1>{$user->getName()} {$user->getSurname()}</h1>";

           foreach($user->getVideos() as $video){
            echo "<p>{$video->getTitle()}</p>";
           }

      
        }

        foreach ($videos as $video){
            echo "{$video->getUrl()}";
        }
        */
//var_dump($videos);
        //return new JsonResponse($users);
        die;
        /*
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);*/
    }

    public function create(request $request){
  


        //recoger los datos por post
        $json = $request->get('json',null);
        
        //decodificar el json
        $params = json_decode($json);
        
        //hacer una respuesta por defecto
        $data = [
            'status' => 'error',
            'code' => 200,
            'message' => 'El usuario no se ha creado',
            'json' => $params,
        ];



        // comprobar y validar datos
        if ($json != null) {
            $name = (!empty($params->name)) ? $params->name : null;
            $surname = (!empty($params->surname)) ? $params->surname : null;
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;

            $validator = Validation::CreateValidator();
            $validate_email = $validator->validate($email, [
                new Email()
            ]);

            if(!empty($email)  && count($validate_email) == 0 && !empty($password) && !empty($name) && !empty($surname)){
                $data = [
                    'status' => 'sucess',
                    'code' => 200,
                    'message' => 'Validacion Correcta',
                    'json' => $params,
                ];
            }else{
                $data = [
                    'status' => 'sucess',
                    'code' => 200,
                    'message' => 'Validacion Incorrecta',
                    'json' => $params,
                ];
            }
            
        }else{
            $data = [
                'status' => 'error',
                'code' => 200,
                'message' => 'El usuario no se ha creado',
                'json' => $params       ,
            ];
        }

        //si la validacion es correcta, crear el objeto del usuario
        $user = new User;
        $user->setName($name);
        $user->setSurname($surname);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setRole('ROLE_USER');
        $user->setCreatedAt(new \Datetime('now'));
        

        //cifrar la contraseña
        $pwd = hash('sha256',$password);
        $user->setPassword($pwd);
        
        //comprobar si el usuario existe (duplicado)
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $user_repo = $doctrine->getRepository(User::class);
        $isset_user = $user_repo->findBy(array(
            'email'  => $email
        ));
        
        //si no existe, guardarlo en la base da datos.
        if(count($isset_user) == 0){
            $em->persist($user);
            $em->flush();

            //guardo el usuario
            $data = [
                'status' => 'access',
                'code' => 400,
                'message' => 'usuario creado correctamente',
                'user' => $user,
            ];
        }else{
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'usuario duplicado',
                'json' => $params       ,
            ];
        };

        //hacer la respuesta en json

        return  new JsonResponse($data);


    }

    public function login(Request $request,JwtAuth $jwt_auth){
 
        //recibir los datos por post
        $json = $request->get('json',null);
        $params = json_decode($json);

        // tener un array por defecto para devolver
        $data = [
            'status' => 'error',
            'code' => 200,
            'message' => 'Usuario no se a podido identificar',
            'params' => $params,
            
        ];
        

        //comprobar y validar date_offset_get
        if ($json != null) {
            $email = (!empty($params->email)) ? $params->email : null;
            $password = (!empty($params->password)) ? $params->password : null;
            $gettoken = (!empty($params->gettoken)) ? $params->gettoken : null;

            $validator = Validation::CreateValidator();
            $validate_email = $validator->validate($email, [
                new Email()
            ]);
            
            if (0==0) {
                //cifrar contraseña
                $pwd = hash('sha256',$password);
                
                //si todo es valido llamraremos a un servicio para identificar el usuario 
                if ($gettoken) {
                    $signup = $jwt_auth->signup($email,$pwd,$gettoken);
                }else{
                    
                    $signup = $jwt_auth->signup($email,$pwd);


                }
                return new JsonResponse($signup);   
            }

        }






        //si devuelve bien los datos retornar respuesta;
        return new JsonResponse($data);

    }

    public function edit(Request $request, JwtAuth $jwt_auth){
        //recoger la cabecera 
        $token = $request->headers->get('Authorization');
        
        //crear un metodo para comprar si el token es correcto 
        $authCheck = $jwt_auth->checkToken($token);

        //respuesta por defecto

        $data = [
            'status' => 'error',
            'code' => 400,
            'message' => 'Usuario no ACTUALIZADO',
            
        ];
  
        
        //si es correcto, hacer la actualizacion de usuario 
        if ($authCheck) {
            //actualizar a l usuario
               


            //conseguir en entyty manager 
            $em = $this->getDoctrine()->getManager();

            //conseguir los datos del usuario idenrtificado
            $identity = $jwt_auth->checkToken($token, true);
           
            //conseguir  el usuarui a actualizar comprleit
            $user_repo = $this->getDoctrine()->getRepository(User::class);
            $user = $user_repo->findOneBy([
                'id' => $identity->sub
            ]);

   


            //recoger datosd por post
            $json = $request->get('json', null);
            $params = json_decode($json);

            //coprobas y validar los datos
            if ($json != null) {
                
                $name = (!empty($params->name)) ? $params->name : null;
                $surname = (!empty($params->surname)) ? $params->surname : null;
                $email = (!empty($params->email)) ? $params->email : null;
    
                $validator = Validation::CreateValidator();
                $validate_email = $validator->validate($email, [
                    new Email()
                ]);
    
                if(!empty($email)  && count($validate_email) == 0&& !empty($name) && !empty($surname)){

                    
                         //asignar nuevdos datos al onjeto del suuario
                    $user->setEmail($email);
                    $user->setName($name);
                    $user->setSurname($surname);


            //comprboar duplicados
                    $isset_user = $user_repo->findBy([
                        'email'=>$email
                    ]);
                    if (count($isset_user) == 0 || $identity->email ==$email ) {
                                    //guardar cambiso en la base de datos

                        $em->persist($user);
                        $em->flush();

                        $data = [
                            'status' => 'succes',
                            'code' => 200,
                            'message' => 'Usuario Actualizado',
                            'user' => $user,
                            
                        ];
                    }else{

                        $data = [
                            'status' => 'error',
                            'code' => 400,
                            'message' => 'No puedes usar ese email',
                            'user' => $user,
                            
                        ];
                    }

                }
            }

       


        }
        
        return new JsonResponse($data);
    }



}
